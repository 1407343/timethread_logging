#include <iostream>
#include <cstdlib>
#include <map> //use for multimap
#include <time.h>
#include <stdio.h>
#include <stdio_ext.h>
#define FLUSH(fp) __fpurge(fp)

extern int ran_range;
//extern "C"

using namespace std;

/*node struct into multimap*/
struct data
{	
	//key value & timeout value
	time_t key; 
	long to_nsec;

	/*current time*/
	int sec; // 0 ~ 60 sec
	int min; // 0 ~ 59 min
	int hour; // 0 ~ 23 hour
	int day; // 1 ~ 31 day
	int mon; // 0 ~ 11 month;
	int year; // since 1900
	int data_num; //data number
	int ran_val; //add random value
	int tos_check; // timeout & signal check
};

/*g_struct data counting & max count*/
typedef struct data_number
{
	int count; //data count
	int make_data_num; //create data num
	int ck_timeout; //ck_smallest timeout
	int to_count;
}data_number;

/*define multimap type*/
typedef multimap < int, data*> wait_data_list; //make data & wait data
typedef multimap < int, data*> out_data_list; //timeout datas list

/*global multimap*/
extern wait_data_list mwa;
extern out_data_list mo;
