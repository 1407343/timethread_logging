#include <iostream>
#include <map>
#include "def_data.hpp"
#include "def_thread.hpp"

int g_ck = 0;//ck scanf result
int ran_range = 0;// global range

wait_data_list mwa;
out_data_list mo;

/*test*/
std::multimap< int, data*>::iterator iter;

/*define init struct data*/
data_number *init_set()
{
	data_number *set = (data_number *)malloc(sizeof(data_number));

	set->count = 0;
	set->make_data_num = 0;
	set->ck_timeout = 0;

	return set;
}

int main()
{
	/*init setting*/
	data_number *set = init_set();

	pthread_t create;//create data
	pthread_t move_data; //timeout move data

	//make data thread
	pthread_create(&move_data, NULL, timeout_data, (void*)set);

	while(1)
	{
		int menu_num = 0;
		int c = 0;
		while(1)
		{
			printf("what do you want\n"
				"select menu\n"
				"=================\n"
				"1. add mode\n"
				"2. delete mode\n"
				"3. find mode\n"
				"4. exit\n"
				"=================\n");

			printf("menu ===> ");
			c = scanf("%d",&menu_num);
		
			/*erase buffer*/
			if(c != 0 && menu_num > 0)//success number data
				break;
			FLUSH(stdin);
			printf("only insert number please\n");
		}
		switch(menu_num)
		{
			//making data
			case 1:
			{
				while(1)
				{
					printf("\nHow many want to make data? =>");
					g_ck = scanf("%d",&set->make_data_num);

					/*erase buff*/
					if(g_ck != 0 && 0 < set->make_data_num)
						break;
					FLUSH(stdin);
					printf("only insert number and 0 over number\n");
				}
	
				while(1)
				{
					printf("\nwhat do you want random range? =>");
					g_ck = scanf("%d",&ran_range);

					/*erase buff*/
					if(g_ck != 0 && -1 < ran_range)
						break;
					FLUSH(stdin);
					printf("\nonly insert number and -1 over number\n");
				}
				pthread_create(&create, NULL, create_data, (void*)set);
				pthread_join(create, NULL);
				set->make_data_num = 0;
				break;
			}
			case 2:
			{
				int delete_num = 0;
				int ck_s = 0;
				int ck_data = 0;
				while(1)
				{
					printf("\nwhat do you want delete data?=>");
					ck_s = scanf("%d",&delete_num);

					/*erase buff*/
					if(ck_s != 0 && 0 < delete_num)
						break;
					FLUSH(stdin);
					printf("only insert number and 0 over number\n");
				}

				pthread_mutex_lock(&mutex);
				for (iter = mwa.begin(); iter != mwa.end(); ++iter)
				{
					if(delete_num == iter->second->data_num)
					{
						iter->second->tos_check = 2;
						mo.insert(make_pair(iter->first, iter->second));

						printf("\n========================\n");
						printf("serch this data!!\n");
						printf("[delete_signal] key : %d, min :%d sec : %d random value : %d data_number : %d \n",iter->first, iter->second->min, iter->second->sec, iter->second->ran_val, iter->second->data_num);
						printf("========================\n");
						mwa.erase( iter );
						ck_data = 1;
						pthread_cond_signal(&tkcond);
					}
				}
				pthread_mutex_unlock(&mutex);
				if(ck_data == 1)
				{
					printf("succes delete this data\n");
					ck_data = 0;
				}
				else
				{
					printf(" We dont have this data\n");
					ck_data = 0;
				}
				break;
			}
			case 3:
			{
				printf("3. find node\n");
				
				printf("what do you want search?\n"
				"1. wait data\n"
				"2. signal data\n"
				"3. timeout data\n");
				
				int fmenu_num = 0;
				int c = 0;
				while(1)
				{
					printf("menu ===> ");
					c = scanf("%d",&fmenu_num);
		
					/*erase buffer*/
					if(c != 0 && menu_num > 0 && menu_num < 4)//success number data
						break;
					FLUSH(stdin);
					printf("only insert number please\n");
				}
				if ( fmenu_num == 1 )
				{
					printf("\nwait data\n");
					pthread_mutex_lock(&mutex);
					for (iter = mwa.begin(); iter != mwa.end(); ++iter)
					{
						cout << "KEY : " << iter->first << " " << "min : " << iter->second->min
							<< " sec : " << iter->second->sec << " rand value : " << iter->second->ran_val << " data number : " << iter->second->data_num << endl;
					}
					pthread_mutex_unlock(&mutex);
					printf("============================\n");
					if(mwa.empty() == 1 )
						printf("non wait data\n");
				}
				else if ( fmenu_num == 2 )
				{
					int ch_data = 0;
					printf("\nsignal datas\n");
					pthread_mutex_lock(&mutex);
					for (iter = mo.begin(); iter != mo.end(); ++iter)
					{
						if(iter->second->tos_check == 2)
						{
							cout << "KEY : " << iter->first << " " << "min : " << iter->second->min
							<< " sec : " << iter->second->sec << " rand value : " << iter->second->ran_val << " data number : " << iter->second->data_num << " delete case 1 is timeout 2 is signal : " << iter->second->tos_check << endl;
							ch_data = 1;
						}
					}
					pthread_mutex_unlock(&mutex);
					printf("============================\n");
					if(ch_data== 0)
						printf("non signal data\n");
				}
				else if ( fmenu_num == 3)
				{
					int ch_data = 0;
					printf("\nout datas\n");
					pthread_mutex_lock(&mutex);
					for (iter = mo.begin(); iter != mo.end(); ++iter)
					{
						if(iter->second->tos_check == 1)
						{
							cout << "KEY : " << iter->first << " " << "min : " << iter->second->min
							<< " sec : " << iter->second->sec << " rand value : " << iter->second->ran_val << " data number : " << iter->second->data_num << " delete case 1 is timeout 2 is signal : " << iter->second->tos_check << endl;
							ch_data = 1;
						}
					}
					pthread_mutex_unlock(&mutex);
					printf("============================\n");
					if(ch_data== 0)
						printf("non out data\n");
				}
				break;
			}
			case 4:
				printf("Exit\n");
				break;
			default:
				printf("Wrong answer\n");
		}
	}

	return 0;
}
