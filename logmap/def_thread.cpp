#include "def_thread.hpp"
#include "def_data.hpp"
#include <cstdlib>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t tkcond = PTHREAD_COND_INITIALIZER; //take out cond

/*ck timeout*/
int status = 0;

/*use for timeout thread*/
struct timeval curtime; //current time

/*read data*/
std::multimap< int, data*>::iterator ckiter;

/*make data*/
void *create_data(void *t_set)
{
	struct timespec ts; //make use for key
	struct timeval curtime; //current time


	struct tm* timeinfo; //time
	int timeout = 0;

	data_number *set = (data_number*)t_set;

	while(1)
	{
		//make timeout range
		if(ran_range == 0)
		{
			timeout = 0;
		}
		else
		{
			timeout = rand() % ran_range + 1;//make max range;
		}

		/*make data num compare maked data numbers*/
		
		pthread_mutex_lock(&mutex);
		
		/*update current time*/
		gettimeofday(&curtime, NULL);//make key & use for timeout thread
		
		//make key value;
		ts.tv_sec = curtime.tv_sec + timeout;
		ts.tv_nsec = curtime.tv_usec * 1000; //unit unity
		
		/*into timeout or signal data wait condition*/

		if(set->count < set->make_data_num)
		{
			set->count++;
			/*assignment struct*/
			data* mkdata = new data();

			/*key value -> 1900.1.1 times sec*/
			mkdata->key = time(NULL);
			
			/*use data current time*/
			timeinfo = localtime(&mkdata->key);
			
			/*key value & timeout value*/
			mkdata->key = ts.tv_sec;
			mkdata->to_nsec = ts.tv_nsec;

			/*into present time data*/
			mkdata->sec = timeinfo->tm_sec;
			mkdata->min = timeinfo->tm_min;
			mkdata->hour = timeinfo->tm_hour + 1;
			mkdata->day = timeinfo->tm_mday;
			mkdata->mon = timeinfo->tm_mon + 1;
			mkdata->year = timeinfo->tm_year + 1900;
			mkdata->data_num = set->count;
			mkdata->ran_val = timeout;
		
			/*into multmap*/
			mwa.insert(make_pair(mkdata->key,mkdata));

			//i'm make data so check time signal
			
			printf("current time : %d \n", (int)curtime.tv_sec);
			printf("[make] key %d min %d sec %d data_num(makenum) %d ranvalue %d data_num %d\n",(int)mkdata->key, mkdata->min, mkdata->sec, mkdata->data_num, mkdata->ran_val, mkdata->data_num);
			
			//i'm make data so check time signal
			pthread_cond_signal(&tkcond);

			pthread_mutex_unlock(&mutex);
		}
		else
		{
			pthread_mutex_unlock(&mutex);
			break;
		}
		
	}

	return NULL;
}

//move timeout data into mo multimap
void *timeout_data(void *data)
{
	struct timespec ckts; //check smallest time struct (change key value)
	data_number *set = (data_number*)data;

	time_t ck_time; //check current time make use for time datas
	
	/*ck timeout*/
	int status = 0;

	while(1)
	{
		//only use check timeout & signal
		status = pthread_cond_timedwait(&tkcond, &mutex, &ckts);

		/*update current time*/
		gettimeofday(&curtime, NULL);
		ck_time = (int)curtime.tv_sec;
		
		if(mwa.empty() == 1 ) //signal checking none data
		{
			continue;
		}
		else
		{
			ckts.tv_sec = mwa.begin()->first; //update timeout value
			ckts.tv_nsec = mwa.begin()->second->to_nsec;
			set->ck_timeout = mwa.begin()->first; //set sturct same ckts.tv_sec
		}
		
		/*timeout data move & delete*/
		ckiter = mwa.begin(); // find first data
		if(status == ETIMEDOUT)
		{
			//checking first data timeout & timeout signal
			if(ckiter->first <= ck_time)
			{
				ckiter->second->tos_check = 1;
				mo.insert(make_pair(ckiter->first, ckiter->second));//move another map
				printf("[timeout] key : %d, min : %d sec : %d random value : %d data_number : %d \n",ckiter->first, ckiter->second->min, ckiter->second->sec, ckiter->second->ran_val, ckiter->second->data_num);
				
				set->to_count++;
				
				mwa.erase( ckiter );//erase first data
				
				if(mwa.empty() != 1) //checking update data first key value is first timeout
				{
					ckts.tv_sec = mwa.begin()->first; //update timeout value
					ckts.tv_nsec = mwa.begin()->second->to_nsec;
					set->ck_timeout = mwa.begin()->first; //set sturct same ckts.tv_sec
				}
				else
				{
					continue;
				}
			}
			else
			{
				printf("data is remove\n");
			}
		}
		else if(status == 0)//signal
		{
			ckts.tv_sec = mwa.begin()->first; //update timeout value
			ckts.tv_nsec = mwa.begin()->second->to_nsec;
			set->ck_timeout = mwa.begin()->first; //set sturct same ckts.tv_sec
		}
	}
	return NULL;
}

///*signal data*/
//void *signal_data(void *data)
//{
//	int delete_num = 0; // you want delete datanumber
//	int ck_s = 0; // check scanf
//	int ck_data = 0; //check success erase
//	int com_go = 0; // you want more delete data
//
//	data_number *set = (data_number*)data;	
//
//	//checking data time
//	while(1)
//	{
//		while(1)
//		{
//			printf("\nDo you want delete data?=> 1. Yes 2. No \n");
//			ck_s = scanf("%d",&com_go);
//
//			/*erase buff*/
//			if(ck_s != 0 && 0 < com_go && com_go < 3)
//				break;
//			FLUSH(stdin);
//			printf("only insert number and 0 over number, range over\n");
//		}
//		if(com_go == 2)
//		{
//			continue;
//		}
//	
//		while(1)
//		{
//			printf("\nwhat do you want delete data?=>");
//			ck_s = scanf("%d",&delete_num);
//
//			/*erase buff*/
//			if(ck_s != 0 && 0 < delete_num && delete_num <= set->count)
//				break;
//			FLUSH(stdin);
//			printf("only insert number and 0 over number, range over\n");
//			printf("range is maybe 1 ~ %d\n",set->count);
//		}
//
//		for (ckiter = mwa.begin(); ckiter != mwa.end(); ++ckiter)
//		{
//			if(delete_num == ckiter->second->data_num)
//			{
//				sd.insert(make_pair(ckiter->first, ckiter->second));
//
//				printf("\n========================\n");
//				printf("serch this data!!\n");
//				printf("[delete_signal] key : %d, min :%d sec : %d random value : %d data_number : %d \n",ckiter->first, ckiter->second->min, ckiter->second->sec, ckiter->second->ran_val, ckiter->second->data_num);
//				printf("========================\n");
//				mwa.erase( ckiter );
//				ck_data = 1;
//				set->to_count++;
//			}
//		}
//		if(ck_data == 1)
//		{
//			printf("succes delete this data\n");
//			ck_data = 0;
//		}
//		else
//		{
//			printf(" We dont have this data\n");
//			ck_data = 0;
//		}
//	}//end while this thread
//	return NULL;
//}

///*make data*/
//void *create_data(void *t_set)
//{
//	struct tm* timeinfo; //time
//	int timeout = 0;
//	int data_num = 0;
//
//	data_number *set = (data_number*)t_set;
//
//	while(1)
//	{
//		//random value 0 ~ 30 
//		timeout = rand() % 30;
//		data_num++;
//		/*make data num compare maked data numbers*/
//		
//		pthread_mutex_lock(&mutex);
//		
//		/*update current time*/
//		ck_time = time(NULL); // make node data
//		gettimeofday(&curtime, NULL);//make key & use for timeout thread
//		
//		//make key value;
//		ts.tv_sec = curtime.tv_sec + timeout;
//		ts.tv_nsec = curtime.tv_usec * 1000; //unit unity
//		
//		/*into timeout or signal data wait condition*/
//
//		if(set->count < set->make_data_num)
//		{
//			/*assignment struct*/
//			data* mkdata = new data();
//
//			/*key value -> 1900.1.1 times sec*/
//			mkdata->key = time(NULL);
//			
//			/*use data current time*/
//			timeinfo = localtime(&mkdata->key);
//			
//			/*key value & timeout value*/
//			mkdata->key = ts.tv_sec;
//			mkdata->to_nsec = ts.tv_nsec;
//
//			/*into present time data*/
//			mkdata->sec = timeinfo->tm_sec;
//			mkdata->min = timeinfo->tm_min;
//			mkdata->hour = timeinfo->tm_hour + 1;
//			mkdata->day = timeinfo->tm_mday;
//			mkdata->mon = timeinfo->tm_mon + 1;
//			mkdata->year = timeinfo->tm_year + 1900;
//			mkdata->data_num = data_num;
//		
//			/*into multmap*/
//			mwa.insert(make_pair(mkdata->key,mkdata));
//
//			set->count++; //add data count
//			
//			/*update smallest time / ts is key value & timeout*/
//			//no_data update timeout is set->count == 1 becaues 0->1 use one time
//			if(ckts.tv_sec > ts.tv_sec || set->ck_timeout > (int)ts.tv_sec || set->count == 1)
//			{
//				ckts.tv_sec = ts.tv_sec;
//				ckts.tv_nsec = ts.tv_nsec;
//				set->ck_timeout = (int)mkdata->key;
//			}
//			/*one time wakeup signal thread*/
//			if(set->count == 1)
//			{
//				pthread_cond_signal(&sgcond);
//			}
//
//			pthread_mutex_unlock(&mutex);
//			
//			printf("[make] key %d min %d sec %d \n",(int)mkdata->key, mkdata->min, mkdata->sec);
//			//sleep(1);//make for 1 second
//		}
//		else
//		{
//			pthread_mutex_unlock(&mutex);
//			break;
//		}
//		
//	}
//
//	return NULL;
//}
//
///*make signal thread ck 5 sec*/
//void *cktime_data(void *data)
//{
//	int sigdata = 0; // signal menu number
//	int sgto_start = 0;
//	pthread_cond_wait(&sgcond, &mutex);
//
//	while(1)
//	{
//		if(sgto_start == 1)
//			pthread_cond_timedwait(&sgcond, &mutex, &sigts);
//		cksig++;
//		sigdata = 1;
//		
//		/*select thread*/
//		if(sigdata == 1)
//		{
//			pthread_cond_signal(&tkcond);
//			sigdata = 0;
//		}
//		else if(sigdata == 3)
//		{
//			pthread_mutex_unlock(&mutex);
//			sigdata = 0;
//			return NULL;
//		}
//		gettimeofday(&curtime, NULL);//make key & use for timeout thread
//		
//		//make sigthread call time;
//		sigts.tv_sec = curtime.tv_sec + 3;
//		sigts.tv_nsec = curtime.tv_usec * 1000;
//		
//		//use timedwait	
//		sgto_start = 1;
//		
//		//check non data
//		if(mwa.empty() == 1)
//			break;
//	}
//
//	pthread_mutex_unlock(&mutex);
//	printf("\n[signal thread] : %d tries\n",cksig - 1);
//	return NULL;
//}
////move timeout data into mo multimap
//void *timeout_data(void *data)
//{
//	data_number *set = (data_number*)data;
//	
//	while(1)
//	{
//		status = pthread_cond_timedwait(&tkcond, &mutex, &ckts);
//		/*update current time*/
//		gettimeofday(&curtime, NULL);
//		ck_time = (int)curtime.tv_sec;
//	
//		/*update smallest time maybe add data thread is dead check left data*/
//		if(mwa.begin()->first < ck_time)
//		{
//			ckts.tv_sec = mwa.begin()->first;
//			set->ck_timeout = ckts.tv_sec;
//		}
//
//		/*timeout data move & delete*/
//		if(set->ck_timeout <= ck_time && set->ck_timeout > 0)
//		{
//			ckiter = mwa.begin(); // find first data
//			if(status == ETIMEDOUT)
//			{
//				mo.insert(make_pair(ckiter->first, ckiter->second));//move another map
//				printf("[timeout] key : %d, min : %d sec : %d\n",ckiter->first, ckiter->second->min, ckiter->second->sec);
//			}
//			else if(status == 0)	
//			{
//				ms.insert(make_pair(ckiter->first, ckiter->second));
//				printf("[signal] key : %d, min : %d sec : %d\n",ckiter->first, ckiter->second->min, ckiter->second->sec);
//			}
//			mwa.erase( ckiter ); // erase first data
//
//			//exit thread
//			if(mwa.empty() == 1)
//			{
//				pthread_mutex_unlock(&mutex);
//				return NULL;
//			}
//			
//			set->ck_timeout = (int)mwa.begin()->first; //update timeout
//			ckts.tv_sec = mwa.begin()->first;
//			//printf("check update sec %d\n",(int)mwa.begin()->first);
//			ckts.tv_nsec = mwa.begin()->second->to_nsec;
//		}
//
//		if(set->count >= set->make_data_num && mwa.empty() == 1)
//		{
//			pthread_mutex_unlock(&mutex);
//			break;
//		}
//	}
//
//	return NULL;
//}
