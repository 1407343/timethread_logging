#include "printlog.h"

#define MUTEX_LOCK() pthread_mutex_lock(&gLogMutex);
#define MUTEX_UNLOCK() pthread_mutex_unlock(&gLogMutex);

//global value
static const char* gLoglevel[LL] = { "[D]", "[W]", "[E]", "[S]", "[I]" }; // Log level setting
struct tm* gcurtime; // set current time
time_t gcrtime;
char filepath[30] = { 0, }; // filepath
FILE* fp; //filepath into write
int glen; //check file size
int gsetloglevel = 0; // setting log level
pthread_mutex_t gLogMutex = PTHREAD_MUTEX_INITIALIZER; // logmutex

//init log function
int initLog()
{
	gcrtime = time(NULL);
	gcurtime = localtime(&gcrtime); // update current time
	gsetloglevel = 3;// setting log level 0 : test / 1 : default except / 2 : erro waring etc.. / 5 : information

	//file name is current time
	sprintf(filepath, "./save/%d%02d%02d_%02d%02d%02d.log", gcurtime->tm_year + 1900, gcurtime->tm_mon + 1, gcurtime->tm_mday, gcurtime->tm_hour, gcurtime->tm_min, gcurtime->tm_sec);

	fp = fopen(filepath, "w+");

	return 0;
}

//current time to string
char *timestring()
{
	struct tm* current;
	static char date[50] = { 0, };
	time_t crtime;

	crtime = time(NULL);
	current = localtime(&crtime);
	
	sprintf(date, "%d%02d%02d_%02d%02d%02d",current->tm_year + 1900, current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);

	return date;
}

//print log function(lglevel is loglevel
int PrintLog(int lglevel, char* log_content, char* logtime)
{
	MUTEX_LOCK();
	
	// over file range
	// this form mutex lock because i dont want same time make 2 files!
	if(glen > MB2)
	{
		gcrtime = time(NULL);
		gcurtime = localtime(&gcrtime); // update current time
		sprintf(filepath, "./save/%d%02d%02d_%02d%02d%02d.log", gcurtime->tm_year + 1900, gcurtime->tm_mon + 1, gcurtime->tm_mday, gcurtime->tm_hour, gcurtime->tm_min, gcurtime->tm_sec);
		
		fp = fopen( filepath, "w+");
		glen = 0; //reset length value
	}
	
	// loglevel print
	if( lglevel >= gsetloglevel )
	{
		glen += fprintf(fp, "%s %s[%d]\n" , logtime, gLoglevel[lglevel], (int)pthread_self()); //use loglevel & thread id

		glen += fprintf(fp, "%s %s %s \n\n" , logtime, gLoglevel[lglevel], log_content); // use loglevel & contentsi
	}

	MUTEX_UNLOCK();

	return 0;
}
