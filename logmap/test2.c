#include <pthread.h>
#include <errno.h>
#include <time.h>

int main(void)
{
	int status;
    struct timeval  curtime;
    struct timespec timeout;

    if (pthread_mutex_lock(&mutex) != 0)
    {
        printf("thread failed to get the mutex\n");
        return;
    }

    gettimeofday(&curtime, NULL);

    timeout.tv_sec = curtime.tv_sec + TIMEOUT;
    timeout.tv_nsec = curtime.tv_usec * 1000;

    status = pthread_cond_timedwait(cond, &mutex, timeout);

    if (status == ETIMEDOUT)
    {
        printf("timed out\n");
    }

	return 0;
}
