#include <pthread.h> //use for thread
#include <unistd.h>
#include <sys/time.h> //use for gettimeofday 
#include <errno.h> //use for error value (timeout thread error value check)

void *create_data(void *t_set); //create map data
void *timeout_data(void *data); //timeout map data save

extern pthread_mutex_t mutex;
extern pthread_cond_t tkcond;
